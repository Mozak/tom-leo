
# -*-coding:UTF-8 -*
import os
import time
def first_index_line(donnee, indice):
	'''return index of the first char of the line (format csv
	'''
	
	char = " "
	while(char != "\n"):
		char = donnee[indice]
		indice -=1
	indice+=2
	return indice
def last_index_line(donnee,index):
	'''renvoie le dernier index de la ligne (format csv)'''
	char = ' '
	while(char != "\n"):
		char = donnee[index]
		index += 1
	return index-1
		
def list_index_line(donnee):
	'''renvoie la liste des premiers indices de toutes les ligne du fichier (format csv)'''
	indice = 0
	list_indice=[]
	i=0
	temps= 0
	taille = len(donnee)
	while(indice < taille):
		list_indice.append(first_index_line(donnee,indice))
		indice = last_index_line(donnee, indice)+1
		i+=1
	return list_indice 
def data_from_line(donnee, indice):
	'''renvoie la liste des datas de chaque colonne (exemple: data de la colonne 2 data_from_line[2]) "indice"'''
	indice_debut = first_index_line(donnee, indice)
	indice_fin = last_index_line(donnee, indice)
	liste_data_line = []
	j=0
	data = ""
	for i in range(indice_debut,indice_fin+1):
		if donnee[i] == ",":
			liste_data_line.append(data)
			data = ""
		else:
			data+= donnee[i]
	return liste_data_line
def dict_of_data_column(donnee, colonne):
	'''return the list of data from the column in a dectionary of the form ("first index of line":"data")''' 
	liste_index=list_index_line(donnee)
	dict_data = {}
	for i in liste_index:
		dict_data[i] = data_from_line(donnee,i)[colonne]
	return(dict_data)

def list_of_a_data(donnee, colonne, valeur):
	'''retourne une liste de tout les premier indice de chaque ligne où l'on trouve valeur dans la colonne "colonn"'''
	dict_of_data = dict_of_data_column(donnee,colonne)
	liste_indice = []
	for i in dict_of_data:
		if dict_of_data[i] == valeur:
			liste_indice.append(i)
	return liste_indice
def ligne_entiere(donnee, indice):
	'''return the entire line where the char[indice] is (format csv)'''
	indice_debut = first_index_line(donnee,indice)
	char = " "
	ligne = ""
	i=indice_debut
	while(char != "\n"):
		char = donnee[i]
		i+=1
	indice_fin = i-1
	for i in range(indice_debut,indice_fin):
		ligne+=donnee[i]
	return ligne
temps=time.time()

os.chdir("/home/nem/Téléchargements/bano-data-master")
donnee = ""
for i in range(2,9):
	fichier = open("bano-0{0}.csv".format(i),"r")
	donnee += fichier.read()
	fichier.close()
for i in range(10,12):
	if i != 20:
		os.chdir("/home/nem/Téléchargements/bano-data-master")
		fichier = open("bano-{0}.csv".format(i),"r")
		donnee = fichier.read()
		fichier.close()
		chaine = ""
		liste = list_index_line(donnee)	
		for i in liste :
			chaine += str(ligne_entiere(donnee,i))+"\n"
			#print(chaine)
		os.chdir("/home/nem/Bureau/pythonWeb")
		fichier = open("liste_rue.csv","a")
		fichier.write(chaine)
		fichier.close()
liste = list_of_a_data(donnee,6,"Rue Lino Ventura")
#for i in liste:
#	print(ligne_entiere(donnee,i))
#	print(len(liste))
print(time.time()-temps)
