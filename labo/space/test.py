import pygame
from math import *
import time
import random

size = width, height = 1600, 800
screen = pygame.display.set_mode(size)
planete_img = [pygame.image.load('planete1.bmp').convert(), pygame.image.load('planete2.bmp').convert(), pygame.image.load('planete3.bmp').convert(), pygame.image.load('planete4.bmp').convert(),pygame.image.load('planete5.bmp').convert(),pygame.image.load('planete6.bmp').convert(), pygame.image.load('planete7.bmp').convert(), pygame.image.load('planete8.bmp').convert(),pygame.image.load('planete9.bmp').convert(),pygame.image.load('planete10.bmp').convert() ]
planete = [[[0,-500],[5,0],100.0], [[0,0],[0,0],70000.0],[[400,500],[-4,0],1000.0]]
planete_rect = []
def creation_random(nombre_planete, nombre_soleil, zoom):
	planeteL = []
	for i in range(nombre_planete):
		planeteL.append([[random.randint((-size[0]/2)*zoom,(size[0]/2)*zoom),random.randint((-size[1]/2)*zoom,(size[1]/2)*zoom)],[random.uniform(-2,2),random.uniform(-2,2)],random.uniform(0,100),[0,0]])
	for j in range(nombre_soleil):
		planeteL.append([[random.randint((-size[0]/2)*zoom,(size[0]/2)*zoom),random.randint((-size[1]/2)*zoom,(size[1]/2)*zoom)],[0,0],random.uniform(50000,100000),[0,0]])
	print(planeteL)	
	return planeteL
def deplacement(planete):
	for i in planete:
		for j in planete:
			if i == j :
				pass
			else:
				d = sqrt(((i[0][0] - j[0][0])*(i[0][0] - j[0][0])) + ((i[0][1] -j[0][1])* (i[0][1] -j[0][1]))) #calcul de la distance entre les 2 planetes
				d = d
				direction_force = (((j[0][0] - i[0][0])/d), ((j[0][1] - i[0][1])/d)) #calculdu vecteur directeur de la force
				acceleration = (((i[2] * j[2])/(d*d)))                               #calcul de l'acceleration
				i[1][0] = ((1/i[2])* acceleration * direction_force[0] + i[1][0])
				i[1][1] = ((1/i[2])* acceleration * direction_force[1] + i[1][1])
				i[3][0] = ( (1/i[2])* acceleration * direction_force[0])
				i[3][1] = ((1/i[2])*  acceleration * direction_force[1])
				i[0][0] = i[0][0] + i[1][0]
				i[0][1] = i[0][1] + i[1][1] 
	return planete
def dessin_vecteur(planete, planeterect):
	for i in range(len(planete)):
		pygame.draw.line(screen,(255,255,255),(planeterect[i].x+5,planeterect[i].y+5),(planeterect[i].x+5+(planete[i][3][0]*10000),planeterect[i].y+5+(planete[i][3][1]*10000)))
 		pygame.draw.line(screen,(255,0,0),(planeterect[i].x+5,planeterect[i].y+5),(planeterect[i].x+5+(planete[i][1][0]*100),planeterect[i].y+5+(planete[i][1][1]*100)))

def affichage (planete, planeterect,zoom):
	for i in range(len(planete)) :

		planeterect[i].x = int((size[0]/2)+(planete[i][0][0]/zoom))
		planeterect[i].y = int((size[1]/2)+(planete[i][0][1]/zoom))
		screen.blit(planete_img[(i%len(planete_img))], planeterect[i])


screen.fill((0,0,0))

def main(planete):
	
	for i in range(len(planete)):
		planete_rect.append(planete_img[(i%(len(planete_img)))].get_rect())
	while(1):
		for event in pygame.event.get():
			if event.type == pygame.QUIT: sys.exit()
	

		planete = deplacement(planete)
		screen.fill((0,0,0))
		affichage(planete, planete_rect,20)
		dessin_vecteur(planete, planete_rect)		
		pygame.display.flip()
main(creation_random(3,1,5))

